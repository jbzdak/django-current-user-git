This is a git clone of hg repo
==============================

Clone of: https://bitbucket.org/aptivate/django-current-user. 

Original readme: 
================

django-current-user enables tracking of the user who last modified or created a model intance

A model with a ``CurrentUserField`` is automatically populated with the current user when an instance is saved.
A model with a ``CreateUserField`` is automatically populated with the current user when an instance is created.

It has been tested to work in django 1.1.X.



Install
=======

Download the tar.gz, extract it and run the following inside the directory:
    python setup.py install

Basic usage
===========

add the appropriate middleware::

    MIDDLEWARE_CLASSES = (
        'current_user.middleware.CurrentUserMiddleware',
        'current_user.middleware.CreateUserMiddleware',
    )

add current_user to your INSTALLED_APPS::

    INSTALLED_APPS = (
        'current_user',
        .
        .
    )


add the fields to your models::


    class ExampleModel(models.Model):

        foo = models.IntegerField()
        last_user = CurrentUserField(related_name="+")
        created_by = CreateUserField(related_name="+")

Tests
=====

To run the tests run the current_user/tests/runtests.py script 

 src/current_user/tests/runtests.py

Example
=======

