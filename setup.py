from distutils.core import setup
import os

# compile the list of packages available, because distutils doesn't have an easy way to do this
packages, data_files = [], []
root_dir = os.path.dirname(__file__)
if root_dir:
    os.chdir(root_dir)

for dirpath, dirnames, filenames in os.walk('current_user'):
    # ignore dirnames that start with '.'
    for i, dirname in enumerate(dirnames):
        if dirname.startswith('.'):
            del dirnames[i]
    if '__init__.py' in filenames:
        pkg = dirpath.replace(os.path.sep, '.')
        if os.path.altsep:
            pkg = pgk.replace(os.path.altsep, '.')
        packages.append(pkg)
    elif filenames:
        # strip 'current_user/' or 'current_user\'
        prefix = dirpath[13:]
        for f in filenames:
            data_files.append(os.path.join(prefix, f))

setup(name='current_user',
      version='1.0',
      description='Store the currently logged in Django auth.User to a model using a FK at time of model saves.',
      author='Marty Alchin',
      maintainer='Corey Bertram',
      maintainer_email='corey@qr7.com',
      url='http://bitbucket.org/q/django-current-user',
      package_dir={'current_user': 'current_user'},
      packages=packages,
      package_data={'current_user': data_files},
      )

