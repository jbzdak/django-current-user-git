from django.db import models
from current_user.models import CurrentUserField
from current_user.models import CreateUserField

class ExampleModel(models.Model):
    """ an example model for testing """

    foo = models.IntegerField()
    last_user = CurrentUserField(related_name="+")
    created_by = CreateUserField(related_name="+")

class ExampleProxy(ExampleModel):

    class Meta:
        proxy=True
