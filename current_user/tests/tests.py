from django.test import TestCase
from current_user.tests.requestfactory import RequestFactory
from current_user.middleware import CurrentUserMiddleware
from current_user.middleware import CreateUserMiddleware
from django.contrib.auth.models import User
from current_user.tests.models import ExampleModel
from current_user.tests.models import ExampleProxy
from django.http import HttpResponse
from django.db.models import signals

class TestSetup(object):

    def setUp(self):

        self.factory = RequestFactory()
        (self.user,created) = User.objects.get_or_create(username='davros')
        signals.pre_save.receivers = []


class TestCurrentUserMiddleware(TestSetup,TestCase):


    def test_middleware_adds_user_to_objects_current_user_field_on_create(self):
        """ 
        middleware adds request.user to a models CurrentUserField on 
        object creation
            
        """

        request = self.factory.put('/some/url')
        request.user = self.user

        middleware= CurrentUserMiddleware()
        middleware.process_request(request)

        # after request goes through the middleware object creation on the example
        # model should set the objects CurrentUserField to request.user

        example = ExampleModel.objects.create(foo=1293)
        self.assertEquals(example.last_user,self.user)
        response = HttpResponse()

        self.assertEquals(len(signals.pre_save.receivers),1)
        middleware.process_response(request,response)

        # ensure the receiver gets disconnected from the signal
        self.assertEquals(signals.pre_save.receivers,[])


    def test_proxymodel_current_user(self):
        """ CurrentUser Middleware works with proxy models """

        request = self.factory.put('/some/url')
        request.user = self.user

        middleware= CurrentUserMiddleware()
        middleware.process_request(request)

        example = ExampleProxy.objects.create(foo=1293)
        self.assertEquals(example.last_user,self.user)


    def test_unique_receiver_assigned_for_active_requests(self):
        """ each active request is assigned a unique receiver function """

        receivers = signals.pre_save.receivers
        middleware = CurrentUserMiddleware()

        requests = []
        for i in range(2):
            request = self.factory.put('/some/url')
            request.user = self.user
            requests.append(request)
        
        self.assertEquals(receivers,[])
        for request in requests:
            middleware.process_request(request)
        
        self.assertEquals(len(receivers),2)
        self.assertNotEqual(receivers[0][1],receivers[1][1])


class TestCreateUserMiddleware(TestSetup,TestCase):


    def test_middleware_adds_user_to_objects_current_user_field_on_create(self):
        """ 
        middleware adds request.user to a models CreateUserField on 
        object creation
            
        """

        request = self.factory.put('/some/url')
        request.user = self.user

        middleware = CreateUserMiddleware()
        middleware.process_request(request)

        example = ExampleModel.objects.create(foo=1293)
        self.assertEquals(example.created_by,self.user)

        response = HttpResponse()

        self.assertEquals(len(signals.pre_save.receivers),1)
        middleware.process_response(request,response)
        self.assertEquals(signals.pre_save.receivers,[])

    
    def test_proxymodel_create_user(self):
        """ CreateUser Middleware works with proxy models """

        request = self.factory.put('/some/url')
        request.user = self.user

        middleware= CreateUserMiddleware()
        middleware.process_request(request)

        example = ExampleProxy.objects.create(foo=1293)
        self.assertEquals(example.created_by,self.user)


class  TestCombinedMiddleware(TestSetup,TestCase):


    def test_middleware_combined(self):
        """ CurrentUser and CreateUser Middleware play nice with each other"""

        request = self.factory.put('/some/url')
        request.user = self.user

        middlewares = (CreateUserMiddleware(),CurrentUserMiddleware())

        for middleware in middlewares:
            middleware.process_request(request)

        example = ExampleModel.objects.create(foo=1293)
        self.assertEquals(example.created_by,self.user)
        self.assertEquals(example.last_user,self.user)

        response = HttpResponse()

        self.assertEquals(len(signals.pre_save.receivers),2)

        for middleware in middlewares:
            middleware.process_response(request,response)

        self.assertEquals(signals.pre_save.receivers,[])
