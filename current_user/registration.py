class FieldRegistryMeta(type):

    def __init__(cls,name,bases,attr):
        """
        create registry for the class being constructed. This is done in at the
        metaclass level to ensure subclasses of FieldRegistry maintain their own
        registry

        """
        cls._registry = {}


class FieldRegistry(object):
    """
    tracks fields attached to models.

    >>> r = FieldRegistry()
    >>> r.add_field('model','f1')
    >>> r.add_field('model','f2')
    >>> r.get_fields('model')
    ['f1', 'f2']

    new instances of the FieldRegistry refer to the same registry state
    >>> r2 = FieldRegistry()
    >>> r.get_fields('model')
    ['f1', 'f2']

    new subclasses maintain their own state
    >>> NewFieldRegistry = type('NewFieldRegistry',(FieldRegistry,),{})
    >>> r = NewFieldRegistry()
    >>> r.items()
    []

    """

    __metaclass__ = FieldRegistryMeta

    def add_field(self, model, field):
        reg = self.__class__._registry.setdefault(model,[])
        reg.append(field)

    def get_fields(self, model):
        return self.__class__._registry.get(model, [])


    def items(self):
        return self.__class__._registry.items()


    def __contains__(self, model):
        return model in self.__class__._registry


class CurrentUserFieldRegistry(FieldRegistry):
    pass

class CreateUserFieldRegistry(FieldRegistry):
    pass


def _test():
    import doctest
    doctest.testmod()

if __name__ == "__main__":
    _test()
