from django.db import models
from django.contrib.auth.models import User

from current_user import registration


class CurrentUserField(models.ForeignKey):

    FieldRegistry = registration.CurrentUserFieldRegistry

    def __init__(self, **kwargs):
        if "to" in kwargs.keys():
            del kwargs["to"]
        if "null" in kwargs.keys():
            del kwargs["null"]
        super(CurrentUserField, self).__init__(User, null=True, **kwargs)

    def contribute_to_class(self, cls, name):
        super(CurrentUserField, self).contribute_to_class(cls, name)
        registry = self.FieldRegistry()
        registry.add_field(cls, self)



class CreateUserField(CurrentUserField):
    FieldRegistry = registration.CreateUserFieldRegistry


