from django.db.models import signals
from django.utils.functional import curry
from django.utils.decorators import decorator_from_middleware

from current_user import registration

class CurrentUserMiddleware(object):
    """
    Middleware which allows any ``CurrentUserField``s on a model instance
    to be updated on ``save()`` this can be used to track who last modified
    a model instance

    """

    FieldRegistry = registration.CurrentUserFieldRegistry

    def process_request(self, request):
        if request.method in ('GET', 'HEAD', 'OPTIONS', 'TRACE'):
            # this request shouldn't update anything
            # so no signal handler should be attached
            return

        if hasattr(request, 'user') and request.user.is_authenticated():
            user = request.user
        else:
            user = None

        update_users = curry(self.update_users, user)
        signals.pre_save.connect(update_users, dispatch_uid=self.dispatch_uid(request), weak=False)


    def dispatch_uid(self,request):
        """ return a unique dispatch uid for the given ``request`` """

        return '%s.%s %s' % ( self.__module__,self.__class__,id(request))


    def update_users(self, user, sender, instance, **kwargs):

        registry = self.FieldRegistry()

        if sender._meta.proxy:
            sender = sender._meta.proxy_for_model

        if sender in registry:
            for field in registry.get_fields(sender):
                setattr(instance, field.name, user)

    def process_response(self, request, response):
        signals.pre_save.disconnect(dispatch_uid=self.dispatch_uid(request))
        return response


class CreateUserMiddleware(CurrentUserMiddleware):
    """
    Middleware which allows any ``CreateUserField``s on a model instance
    to be set on the initial ``save()`` (instance creation)
    this can be used to track who created a model instance

    """

    FieldRegistry = registration.CreateUserFieldRegistry

    def update_users(self,user,sender,instance,**kwargs):
        if getattr(instance,'id',None) is not None:
            return
        else:
            super(CreateUserMiddleware,self).update_users(user,sender,instance,**kwargs)


record_create_user  = decorator_from_middleware(CreateUserMiddleware)
record_current_user = decorator_from_middleware(CurrentUserMiddleware)
